public class Toy{
 private int price;
 private String name;
 private String type;
 
 public Toy(int price, String name){
   this.price = price;
   this.name = name;
 }
 
 public void toyType(){
  System.out.println(this.price);
  
 }
 
 public int getPrice(){
  return this.price;
 }
 
 public String getName(){
  return this.name;
 }
 
 public String getType(){
  return this.type;
 }
 
 /*public void setPrice(int price){
  if (price >= 0){
   this.price = price;
  }
 }
 
 public void setName(String name){
  this.name = name;
 }*/
 
 public void setType(String type){
  this.type = type;
 }
}